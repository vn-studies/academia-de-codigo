public class Main {
    public static void main(String[] args) {
        // Test if it doesn't throw with negative number
        missingChar("Robocop", -7);

        // Test if it removes first letter
        missingChar("Robocop", 0);

        // Test if it removes letter on the 4th position
        missingChar("Robocop", 3);

        // Test if it removes the last letter
        missingChar("Robocop", 6);

        // Tests if nothing is removed and no error is thrown since the index position doesn't exist
        missingChar("Robocop", 7);

        //receive command line argument if available
        if(args.length>=2){
            missingChar(args[0], Integer.parseInt(args[1]));
        }
    }

    private static void missingChar(String str, int n) {

        if(str.length() - 1 < n || n < 0){
            System.out.println(str);
        } else {
            String result = str.substring(0, n) + str.substring(n + 1);
            System.out.println(result);
        }
    }
}